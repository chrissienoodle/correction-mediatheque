<?php
if (!defined('ABSPATH')){
    exit;
}
get_header();

if (have_posts()):
    while (have_posts()):
        the_post();
?>
<div>
    <div class='single-thumbnail'
         style='background-image: url("<?php the_post_thumbnail_url('full'); ?>")'
    >
    </div>
    <div class='single-content'>
        <h1><?php the_title()?></h1>
        <p>
            <?php if(get_field('auteur')):
                echo get_field('auteur');
            endif;
            if(get_field('date_publication') && get_field('auteur')):
                echo ' // ';
            endif;

            if(get_field('date_publication')):
                echo get_field('date_publication');
            endif;
            ?>
        </p>
        <p>
            <?php if(get_field('pages')):
                echo get_field('pages').' pages';
            endif;?>
        </p>
        <?php the_content(); ?>
        <?php if(get_field('galerie')):
            echo get_field('galerie');
        endif;?>
        <p>
            <?php if(get_field('edition')):
                echo 'éd: '.get_field('edition');
            endif;?>
        </p>
        <hr>
        <h2><?php echo __('Commentaire des documentalistes', 'mediastjamme'); ?></h2>
        <?php if(get_field('note')): ?>
            <div>
                <?php for ($i=0; $i<get_field('note'); $i++): ?>
                    <img class='single-rating'
                        src='<?php echo get_stylesheet_directory_uri().'/img/star.svg'; ?>'>
                <?php endfor;?>
            </div>
        <?php endif; ?>
        <?php if(get_field('comment')):
            echo get_field('comment');
        endif; ?>
    </div>



</div>

<?php
    endwhile;
endif;

get_footer();
