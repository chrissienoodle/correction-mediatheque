<?php
if (!defined('ABSPATH')){
    exit;
}
get_header();

if (have_posts()):
    while (have_posts()):
        the_post();
?>
<div>
    <div class='single-thumbnail'
         style='background-image: url("<?php the_post_thumbnail_url('full'); ?>")'
    >
    </div>
    <div class='single-content'>
        <h1><?php the_title()?></h1>
        <p>
            <?php if(get_field('artiste')):
                echo '<strong>Artiste: </strong>'.get_field('artiste');
            endif;
            ?>
        </p>
        <p>
            <?php echo '<strong>Date de sortie: </strong>'.get_field('date'); ?>
        </p>
        <?php the_content(); ?>
        <hr>
        <h2>Liste des pistes</h2>
        <?php if(get_field('pistes')):
            echo get_field('pistes');
        endif; ?>
    </div>



</div>

<?php
    endwhile;
endif;

get_footer();
