<?php

//enqueue style for mediastjamme child theme
add_action('wp_enqueue_scripts', 'enqueue_mediastjamme_style');
if(!function_exists('enqueue_mediastjamme_style')){
    function enqueue_mediastjamme_style(){
        wp_enqueue_style('cc_mediastjamme_style',
        get_stylesheet_directory_uri().'/style.css',
        array('generatepress'),
        wp_get_theme()->get('Version')
        );
    }
}

include 'inc/cpt.php';
include 'inc/cf.php';
include 'inc/taxonomy.php';
