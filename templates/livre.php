<?php
?>
<article class='single-container'>
    <div class='single-thumbnail'
         style='background-image: url("<?php the_post_thumbnail_url('medium'); ?>")'>

    </div>
    <div class='single-content'>
        <h3><?php the_title(); ?></h3>
        <p><?php the_excerpt(); ?></p>
        <a href='<?php the_permalink(); ?>'>Feuilleter</a>
    </div>
</article>
