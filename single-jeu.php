<?php
if (!defined('ABSPATH')){
    exit;
}
get_header();

if (have_posts()):
    while (have_posts()):
        the_post();
?>
<div>
    <div class='single-thumbnail'
         style='background-image: url("<?php the_post_thumbnail_url('full'); ?>")'
    >
    </div>
    <div class='single-content'>
        <h1><?php the_title()?></h1>
        <p>
            <?php if(get_field('age')):
                echo 'A partir de '.get_field('age').' ans';
            endif;
            ?>
        </p>
        <p>
            <?php if (get_field('joueurs_min') && get_field('joueurs_max')):
                echo 'Pour '.get_field('joueurs_min').' à '.get_field('joueurs_max').' joueurs';
            elseif (get_field('joueurs_min')):
                echo 'Pour '.get_field('joueurs_min').' joueurs minimum';
            elseif (get_field('joueurs_max')):
                echo 'Pour '.get_field('joueurs_max').' joueurs maximum';
            endif;?>
        </p>
        <?php the_content(); ?>
        <p>
            <?php if(get_field('duree')):
                echo 'Durée moyenne d\'une partie: '.str_replace(':', 'h', get_field('duree'));
            endif; ?>
        </p>

    </div>



</div>

<?php
    endwhile;
endif;

get_footer();
