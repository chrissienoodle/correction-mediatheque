<?php
if (!defined('ABSPATH')){
    exit;
}
get_header();

if (have_posts()):
    while (have_posts()):
        the_post();
?>
<div>
    <div class='single-thumbnail'
         style='background-image: url("<?php the_post_thumbnail_url('full'); ?>")'
    >
    </div>
    <div class='single-content'>
        <h1><?php the_title()?></h1>
        <p>
            <?php if(get_field('debut')):
                echo '<strong>Date de l\'événement: </strong>'.get_field('debut');
            endif;
            ?>
        </p>
        <p>
            <?php if(get_field('duree')):
                echo '<strong>Durée: </strong>'.get_field('duree');
            endif;
            ?>
        </p>
        <?php the_content(); ?>
        <hr>
        <h2>Liste des intervenants</h2>
        <?php if(get_field('intervenants')):
            echo get_field('intervenants');
        endif; ?>
    </div>



</div>

<?php
    endwhile;
endif;

get_footer();
