<?php

if ( !defined('ABSPATH') ) {
    exit;
}

get_header(); ?>
    <div class='archive-content'>
        <div class='archive-header'
             style='background-image: url("<?php echo get_stylesheet_directory_uri() . '/img/livre.jpg' ?>")'>
            <h1>Bienvenue à la bibliothèque de Sainte-Jamme</h1>
            <p>Tous nos livres sont référencés sur cette page</p>
            <p>Naviguez ... Feuilletez ... Découvrez ...<br>
               Et n'hésitez pas à emprunter&nbsp;!</p>
        </div>

        <div class='archive-container'>
            <div class='archive-column'>
                <h2>Naviguer par genre</h2>
                <?php $terms = get_terms(
                    array(
                        'taxonomy'=>'genre_livre'
                    )
                );

                if (!empty($terms)):?>
                    <ul class='archive-terms'>
                        <?php foreach ($terms as $term): ?>
                            <li>
                                <a href='/<?php echo $term->taxonomy;?>/<?php echo $term->slug;?>'>
                                    <?php echo $term->name; ?>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>

                <h2>Livre choisi par les documentalistes</h2>

                <?php

                $args = array(
                    'post_type' => 'livre',
                    'orderby'   => 'rand',
                    'posts_per_page' => 1,
                );

                $query = new WP_Query($args);

                if ($query->have_posts()):
                    while ($query->have_posts()):
                        $query->the_post();
                        include('templates/livre.php');
                    endwhile;
                endif;
                wp_reset_postdata();
                ?>

            </div>
            <div class='archive-posts'>
                <?php if ( have_posts() ):
                    while ( have_posts() ):
                        the_post();
                        include('templates/livre.php');
                    endwhile;
                endif;
                ?>
            </div>
            <div class='pagination'>
                <?php posts_nav_link(); ?>
            </div>
        </div>
    </div>
<?php get_footer();
