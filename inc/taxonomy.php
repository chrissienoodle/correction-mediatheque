<?php

add_action( 'init', 'create_custom_taxonomies_mediastjamme', 0 );

if (!function_exists('create_custom_taxonomies_mediastjamme')) {
    function create_custom_taxonomies_mediastjamme() {

        $args = array(
            'labels'            => array(
                'name'              => _x( 'Genres', 'taxonomy general name', 'mediastjamme' ),
                'singular_name'     => _x( 'Genre', 'taxonomy singular name', 'mediastjamme' ),
                'search_items'      => __( 'Chercher un statut', 'mediastjamme' ),
                'all_items'         => __( 'Toutes les genres', 'mediastjamme' ),
                'view_item'         => __( 'Voir le genre', 'mediastjamme' ),
                'parent_item'       => __( 'Genre parent', 'mediastjamme' ),
                'parent_item_colon' => __( 'Genre parent:', 'mediastjamme' ),
                'edit_item'         => __( 'Modifier le genre', 'mediastjamme' ),
                'update_item'       => __( 'Modifier le genre', 'mediastjamme' ),
                'add_new_item'      => __( 'Ajouter un nouveau genre', 'mediastjamme' ),
                'new_item_name'     => __( 'Nom du genre', 'mediastjamme' ),
                'not_found'         => __( 'Pas de genre trouvé', 'mediastjamme' ),
                'back_to_items'     => __( 'Retour', 'mediastjamme' ),
                'menu_name'         => __( 'Genre', 'mediastjamme' ),
            ),
            'hierarchical'      => true, // true -> se comporte comme une catégorie // false  -> se comporte comme
            // une étiquette
            'public'            => true,
            'show_ui'           => true, //visible dans l'interface
            'show_admin_column' => true, //visible dans la colone de navigation dans le backoffice
            'query_var'         => true, //permet de modifier des requetes à la base de données
            'rewrite'           => array( 'slug' => 'genre_livre' ),
            'show_in_rest'      => true, //pour l'API
        );
        register_taxonomy('genre_livre', 'livre', $args);

        $args = array(
            'labels'            => array(
                'name'              => _x( 'Genres', 'taxonomy general name', 'mediastjamme' ),
                'singular_name'     => _x( 'Genre', 'taxonomy singular name', 'mediastjamme' ),
                'search_items'      => __( 'Chercher un statut', 'mediastjamme' ),
                'all_items'         => __( 'Toutes les genres', 'mediastjamme' ),
                'view_item'         => __( 'Voir le genre', 'mediastjamme' ),
                'parent_item'       => __( 'Genre parent', 'mediastjamme' ),
                'parent_item_colon' => __( 'Genre parent:', 'mediastjamme' ),
                'edit_item'         => __( 'Modifier le genre', 'mediastjamme' ),
                'update_item'       => __( 'Modifier le genre', 'mediastjamme' ),
                'add_new_item'      => __( 'Ajouter un nouveau genre', 'mediastjamme' ),
                'new_item_name'     => __( 'Nom du genre', 'mediastjamme' ),
                'not_found'         => __( 'Pas de genre trouvé', 'mediastjamme' ),
                'back_to_items'     => __( 'Retour', 'mediastjamme' ),
                'menu_name'         => __( 'Genre', 'mediastjamme' ),
            ),
            'hierarchical'      => true, // true -> se comporte comme une catégorie // false  -> se comporte comme
            // une étiquette
            'public'            => true,
            'show_ui'           => true, //visible dans l'interface
            'show_admin_column' => true, //visible dans la colone de navigation dans le backoffice
            'query_var'         => true, //permet de modifier des requetes à la base de données
            'rewrite'           => array( 'slug' => 'genre_cd' ),
            'show_in_rest'      => true, //pour l'API
        );
        register_taxonomy('genre_cd', 'cd', $args);

        $args = array(
            'labels'            => array(
                'name'              => _x( 'Genres', 'taxonomy general name', 'mediastjamme' ),
                'singular_name'     => _x( 'Genre', 'taxonomy singular name', 'mediastjamme' ),
                'search_items'      => __( 'Chercher un statut', 'mediastjamme' ),
                'all_items'         => __( 'Toutes les genres', 'mediastjamme' ),
                'view_item'         => __( 'Voir le genre', 'mediastjamme' ),
                'parent_item'       => __( 'Genre parent', 'mediastjamme' ),
                'parent_item_colon' => __( 'Genre parent:', 'mediastjamme' ),
                'edit_item'         => __( 'Modifier le genre', 'mediastjamme' ),
                'update_item'       => __( 'Modifier le genre', 'mediastjamme' ),
                'add_new_item'      => __( 'Ajouter un nouveau genre', 'mediastjamme' ),
                'new_item_name'     => __( 'Nom du genre', 'mediastjamme' ),
                'not_found'         => __( 'Pas de genre trouvé', 'mediastjamme' ),
                'back_to_items'     => __( 'Retour', 'mediastjamme' ),
                'menu_name'         => __( 'Genre', 'mediastjamme' ),
            ),
            'hierarchical'      => true, // true -> se comporte comme une catégorie // false  -> se comporte comme
            // une étiquette
            'public'            => true,
            'show_ui'           => true, //visible dans l'interface
            'show_admin_column' => true, //visible dans la colone de navigation dans le backoffice
            'query_var'         => true, //permet de modifier des requetes à la base de données
            'rewrite'           => array( 'slug' => 'genre_jeu' ),
            'show_in_rest'      => true, //pour l'API
        );
        register_taxonomy('genre_jeu', 'jeu', $args);

        $args = array(
            'labels'            => array(
                'name'              => _x( 'types', 'taxonomy general name', 'mediastjamme' ),
                'singular_name'     => _x( 'type', 'taxonomy singular name', 'mediastjamme' ),
                'search_items'      => __( 'Chercher un statut', 'mediastjamme' ),
                'all_items'         => __( 'Toutes les types', 'mediastjamme' ),
                'view_item'         => __( 'Voir le type', 'mediastjamme' ),
                'parent_item'       => __( 'type parent', 'mediastjamme' ),
                'parent_item_colon' => __( 'type parent:', 'mediastjamme' ),
                'edit_item'         => __( 'Modifier le type', 'mediastjamme' ),
                'update_item'       => __( 'Modifier le type', 'mediastjamme' ),
                'add_new_item'      => __( 'Ajouter un nouveau type', 'mediastjamme' ),
                'new_item_name'     => __( 'Nom du type', 'mediastjamme' ),
                'not_found'         => __( 'Pas de type trouvé', 'mediastjamme' ),
                'back_to_items'     => __( 'Retour', 'mediastjamme' ),
                'menu_name'         => __( 'type', 'mediastjamme' ),
            ),
            'hierarchical'      => true, // true -> se comporte comme une catégorie // false  -> se comporte comme
            // une étiquette
            'public'            => true,
            'show_ui'           => true, //visible dans l'interface
            'show_admin_column' => true, //visible dans la colone de navigation dans le backoffice
            'query_var'         => true, //permet de modifier des requetes à la base de données
            'rewrite'           => array( 'slug' => 'type' ),
            'show_in_rest'      => true, //pour l'API
        );
        register_taxonomy('type', 'evenement', $args);

        $args = array(
            'labels'            => array(
                'name'              => _x( 'qualificatifs', 'taxonomy general name', 'mediastjamme' ),
                'singular_name'     => _x( 'qualificatif', 'taxonomy singular name', 'mediastjamme' ),
                'search_items'      => __( 'Chercher un statut', 'mediastjamme' ),
                'all_items'         => __( 'Toutes les qualificatifs', 'mediastjamme' ),
                'view_item'         => __( 'Voir le qualificatif', 'mediastjamme' ),
                'parent_item'       => __( 'qualificatif parent', 'mediastjamme' ),
                'parent_item_colon' => __( 'qualificatif parent:', 'mediastjamme' ),
                'edit_item'         => __( 'Modifier le qualificatif', 'mediastjamme' ),
                'update_item'       => __( 'Modifier le qualificatif', 'mediastjamme' ),
                'add_new_item'      => __( 'Ajouter un nouveau qualificatif', 'mediastjamme' ),
                'new_item_name'     => __( 'Nom du qualificatif', 'mediastjamme' ),
                'not_found'         => __( 'Pas de qualificatif trouvé', 'mediastjamme' ),
                'back_to_items'     => __( 'Retour', 'mediastjamme' ),
                'menu_name'         => __( 'qualificatif', 'mediastjamme' ),
            ),
            'hierarchical'      => false, // true -> se comporte comme une catégorie // false  -> se comporte comme
            // une étiquette
            'public'            => true,
            'show_ui'           => true, //visible dans l'interface
            'show_admin_column' => true, //visible dans la colone de navigation dans le backoffice
            'query_var'         => true, //permet de modifier des requetes à la base de données
            'rewrite'           => array( 'slug' => 'qualificatif' ),
            'show_in_rest'      => true, //pour l'API
        );
        register_taxonomy('qualificatif', 'evenement', $args);
    }
}

