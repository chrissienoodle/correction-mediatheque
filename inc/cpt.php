<?php

add_action('init', 'create_mediastjamme_cpt');
if(!function_exists('create_mediastjamme_cpt')){
    function create_mediastjamme_cpt(){

        $args = array(
            'labels'    => array(
                'name'                  => _x( 'cds', 'Post type general name', 'mediastjamme' ),
                'singular_name'         => _x( 'cd', 'Post type singular name', 'mediastjamme' ),
                'menu_name'             => _x( 'cds', 'Admin Menu text', 'mediastjamme' ),
                'name_admin_bar'        => _x( 'cd', 'Add New on Toolbar', 'mediastjamme' ),
                'add_new'               => __( 'Ajouter un cd', 'mediastjamme' ),
                'add_new_item'          => __( 'Ajouter un nouveau cd', 'mediastjamme' ),
                'new_item'              => __( 'Nouveau cd', 'mediastjamme' ),
                'edit_item'             => __( 'Modifier le cd', 'mediastjamme' ),
                'view_item'             => __( 'Voir le cd', 'mediastjamme' ),
                'all_items'             => __( 'Tous les cds', 'mediastjamme' ),
                'search_items'          => __( 'Chercher les cds', 'mediastjamme' ),
                'parent_item_colon'     => __( 'cds parents:', 'mediastjamme' ),
                'not_found'             => __( 'Pas de cd.', 'mediastjamme' ),
                'not_found_in_trash'    => __( 'Pas de cd dans la corbeillle.', 'mediastjamme' ),
                'featured_image'        => _x( 'Image du cd', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'mediastjamme' ),
                'set_featured_image'    => _x( 'Appliquer l\'image au cd', 'Overrides the “Set featured image” phrase for 
        this post type. Added in 4.3', 'mediastjamme' ),
                'remove_featured_image' => _x( 'Supprimer l\'image du cd', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'mediastjamme' ),
                'use_featured_image'    => _x( 'Utiliser en tant qu\'image du cd', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'mediastjamme' ),
                'archives'              => _x( 'cds', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'mediastjamme' ),
                'insert_into_item'      => _x( 'Ajouter au cd', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'mediastjamme' ),
                'uploaded_to_this_item' => _x( 'Ajouté au cd', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'mediastjamme' ),
                'filter_items_list'     => _x( 'Filtrer les cds', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'mediastjamme' ),
                'items_list_navigation' => _x( 'Naviguer dans la liste des cds', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'mediastjamme' ),
                'items_list'            => _x( 'Liste des cds', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'mediastjamme' ),
            ),
            'public'    => true, //permet de voir le posttype dans l'interface d'admin et en front
            'hierarchical'  => false, //fait que le pst type se comporte comme un article et non une page
            'exclude_from_search'   => false, // le post type apparait dans les résultats de recherche
            'publicly_queryable'    => true, //
            'show_ui'   => true,
            'show_in_menu'  => true,
            'show_in_nav_menus' => true,
            'show_in_admin_bar' => true,
            'show_in_rest'  => true, //Requête ajax (voir avec le cours JS)
            'menu_position' => 6, // sous les articles
            'menu_icon' => 'dashicons-album', //picto dans l'interface d'admin
            'supports'  => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),  //les
            // éléments constituant le post
            'taxonomies'    => array(''),
            'has_archive'   => true,
        );
        register_post_type('cd', $args);

        $args = array(
            'labels'    => array(
                'name'                  => _x( 'livres', 'Post type general name', 'mediastjamme' ),
                'singular_name'         => _x( 'livre', 'Post type singular name', 'mediastjamme' ),
                'menu_name'             => _x( 'livres', 'Admin Menu text', 'mediastjamme' ),
                'name_admin_bar'        => _x( 'livre', 'Add New on Toolbar', 'mediastjamme' ),
                'add_new'               => __( 'Ajouter un livre', 'mediastjamme' ),
                'add_new_item'          => __( 'Ajouter un nouveau livre', 'mediastjamme' ),
                'new_item'              => __( 'Nouveau livre', 'mediastjamme' ),
                'edit_item'             => __( 'Modifier le livre', 'mediastjamme' ),
                'view_item'             => __( 'Voir le livre', 'mediastjamme' ),
                'all_items'             => __( 'Tous les livres', 'mediastjamme' ),
                'search_items'          => __( 'Chercher les livres', 'mediastjamme' ),
                'parent_item_colon'     => __( 'livres parents:', 'mediastjamme' ),
                'not_found'             => __( 'Pas de livre.', 'mediastjamme' ),
                'not_found_in_trash'    => __( 'Pas de livre dans la corbeillle.', 'mediastjamme' ),
                'featured_image'        => _x( 'Image du livre', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'mediastjamme' ),
                'set_featured_image'    => _x( 'Appliquer l\'image au livre', 'Overrides the “Set featured image” phrase for 
        this post type. Added in 4.3', 'mediastjamme' ),
                'remove_featured_image' => _x( 'Supprimer l\'image du livre', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'mediastjamme' ),
                'use_featured_image'    => _x( 'Utiliser en tant qu\'image du livre', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'mediastjamme' ),
                'archives'              => _x( 'livres', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'mediastjamme' ),
                'insert_into_item'      => _x( 'Ajouter au livre', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'mediastjamme' ),
                'uploaded_to_this_item' => _x( 'Ajouté au livre', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'mediastjamme' ),
                'filter_items_list'     => _x( 'Filtrer les livres', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'mediastjamme' ),
                'items_list_navigation' => _x( 'Naviguer dans la liste des livres', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'mediastjamme' ),
                'items_list'            => _x( 'Liste des livres', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'mediastjamme' ),
            ),
            'public'    => true, //permet de voir le posttype dans l'interface d'admin et en front
            'hierarchical'  => false, //fait que le pst type se comporte comme un article et non une page
            'exclude_from_search'   => false, // le post type apparait dans les résultats de recherche
            'publicly_queryable'    => true, //
            'show_ui'   => true,
            'show_in_menu'  => true,
            'show_in_nav_menus' => true,
            'show_in_admin_bar' => true,
            'show_in_rest'  => true, //Requête ajax (voir avec le cours JS)
            'menu_position' => 6, // sous les articles
            'menu_icon' => 'dashicons-book', //picto dans l'interface d'admin
            'supports'  => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),  //les
            // éléments constituant le post
            'taxonomies'    => array(''),
            'has_archive'   => true,
        );
        register_post_type('livre', $args);

        $args = array(
            'labels'    => array(
                'name'                  => _x( 'jeux', 'Post type general name', 'mediastjamme' ),
                'singular_name'         => _x( 'jeu', 'Post type singular name', 'mediastjamme' ),
                'menu_name'             => _x( 'jeux', 'Admin Menu text', 'mediastjamme' ),
                'name_admin_bar'        => _x( 'jeu', 'Add New on Toolbar', 'mediastjamme' ),
                'add_new'               => __( 'Ajouter un jeu', 'mediastjamme' ),
                'add_new_item'          => __( 'Ajouter un nouveau jeu', 'mediastjamme' ),
                'new_item'              => __( 'Nouveau jeu', 'mediastjamme' ),
                'edit_item'             => __( 'Modifier le jeu', 'mediastjamme' ),
                'view_item'             => __( 'Voir le jeu', 'mediastjamme' ),
                'all_items'             => __( 'Tous les jeux', 'mediastjamme' ),
                'search_items'          => __( 'Chercher les jeux', 'mediastjamme' ),
                'parent_item_colon'     => __( 'jeux parents:', 'mediastjamme' ),
                'not_found'             => __( 'Pas de jeu.', 'mediastjamme' ),
                'not_found_in_trash'    => __( 'Pas de jeu dans la corbeillle.', 'mediastjamme' ),
                'featured_image'        => _x( 'Image du jeu', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'mediastjamme' ),
                'set_featured_image'    => _x( 'Appliquer l\'image au jeu', 'Overrides the “Set featured image” phrase for 
        this post type. Added in 4.3', 'mediastjamme' ),
                'remove_featured_image' => _x( 'Supprimer l\'image du jeu', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'mediastjamme' ),
                'use_featured_image'    => _x( 'Utiliser en tant qu\'image du jeu', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'mediastjamme' ),
                'archives'              => _x( 'jeux', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'mediastjamme' ),
                'insert_into_item'      => _x( 'Ajouter au jeu', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'mediastjamme' ),
                'uploaded_to_this_item' => _x( 'Ajouté au jeu', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'mediastjamme' ),
                'filter_items_list'     => _x( 'Filtrer les jeux', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'mediastjamme' ),
                'items_list_navigation' => _x( 'Naviguer dans la liste des jeux', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'mediastjamme' ),
                'items_list'            => _x( 'Liste des jeux', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'mediastjamme' ),
            ),
            'public'    => true, //permet de voir le posttype dans l'interface d'admin et en front
            'hierarchical'  => false, //fait que le pst type se comporte comme un article et non une page
            'exclude_from_search'   => false, // le post type apparait dans les résultats de recherche
            'publicly_queryable'    => true, //
            'show_ui'   => true,
            'show_in_menu'  => true,
            'show_in_nav_menus' => true,
            'show_in_admin_bar' => true,
            'show_in_rest'  => true, //Requête ajax (voir avec le cours JS)
            'menu_position' => 6, // sous les articles
            'menu_icon' => 'dashicons-games', //picto dans l'interface d'admin
            'supports'  => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),  //les
            // éléments constituant le post
            'taxonomies'    => array(''),
            'has_archive'   => true,
        );
        register_post_type('jeu', $args);

        $args = array(
            'labels'    => array(
                'name'                  => _x( 'événements', 'Post type general name', 'mediastjamme' ),
                'singular_name'         => _x( 'événement', 'Post type singular name', 'mediastjamme' ),
                'menu_name'             => _x( 'événements', 'Admin Menu text', 'mediastjamme' ),
                'name_admin_bar'        => _x( 'événement', 'Add New on Toolbar', 'mediastjamme' ),
                'add_new'               => __( 'Ajouter un événement', 'mediastjamme' ),
                'add_new_item'          => __( 'Ajouter un nouveau événement', 'mediastjamme' ),
                'new_item'              => __( 'Nouveau événement', 'mediastjamme' ),
                'edit_item'             => __( 'Modifier le événement', 'mediastjamme' ),
                'view_item'             => __( 'Voir le événement', 'mediastjamme' ),
                'all_items'             => __( 'Tous les événements', 'mediastjamme' ),
                'search_items'          => __( 'Chercher les événementx', 'mediastjamme' ),
                'parent_item_colon'     => __( 'événements parents:', 'mediastjamme' ),
                'not_found'             => __( 'Pas de événement.', 'mediastjamme' ),
                'not_found_in_trash'    => __( 'Pas de événement dans la corbeillle.', 'mediastjamme' ),
                'featured_image'        => _x( 'Image du événement', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'mediastjamme' ),
                'set_featured_image'    => _x( 'Appliquer l\'image au événement', 'Overrides the “Set featured image” phrase for 
        this post type. Added in 4.3', 'mediastjamme' ),
                'remove_featured_image' => _x( 'Supprimer l\'image du événement', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'mediastjamme' ),
                'use_featured_image'    => _x( 'Utiliser en tant qu\'image du événement', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'mediastjamme' ),
                'archives'              => _x( 'événements', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'mediastjamme' ),
                'insert_into_item'      => _x( 'Ajouter au événement', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'mediastjamme' ),
                'uploaded_to_this_item' => _x( 'Ajouté au événement', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'mediastjamme' ),
                'filter_items_list'     => _x( 'Filtrer les événements', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'mediastjamme' ),
                'items_list_navigation' => _x( 'Naviguer dans la liste des événements', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'mediastjamme' ),
                'items_list'            => _x( 'Liste des événements', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'mediastjamme' ),
            ),
            'public'    => true, //permet de voir le posttype dans l'interface d'admin et en front
            'hierarchical'  => false, //fait que le pst type se comporte comme un article et non une page
            'exclude_from_search'   => false, // le post type apparait dans les résultats de recherche
            'publicly_queryable'    => true, //
            'show_ui'   => true,
            'show_in_menu'  => true,
            'show_in_nav_menus' => true,
            'show_in_admin_bar' => true,
            'show_in_rest'  => true, //Requête ajax (voir avec le cours JS)
            'menu_position' => 6, // sous les articles
            'menu_icon' => 'dashicons-megaphone', //picto dans l'interface d'admin
            'supports'  => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' ),  //les
            // éléments constituant le post
            'taxonomies'    => array(''),
            'has_archive'   => true,
        );
        register_post_type('evenement', $args);
    }
}
